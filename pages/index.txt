////
.. title: Welcome
.. type: text
.. hidetitle: True
////

== Welcome to Icebox! ==
Icebox is a small project to make a preconfigured Openbox setup
based upon the Ubuntu base. The goal is not to become an official
flavor or try and replace existing Openbox based distributions.

=== Known issues ===

 * Shutdown/Restart of live session guest does not work in Virtualbox or VMWare - LP: link:http://bugs.launchpad.net/bugs/1447038[#1447038]

=== Features ===

Remember, defaults are just that, default. Feel free to remove or add as you'd like, 
and icebox-desktop is merely a metapackage thus it won't break your install to remove the package.

  * Built from the current Ubuntu release, so large repository of available
    applications.
  * link:http://openbox.org/[Openbox window manager], with little extra configuration
    and link:http://projects.l3ib.org/nitrogen/[Nitrogen] backdrop manager.
  * link:https://code.google.com/p/tint2/[Tint2] panel with gsimplecal calendar applet, gmrun, and volumeicon.
  * link:http://wiki.lxde.org/en/PCManFM[PCManFM] file manager, does not manage the desktop.
  * LXAppearance and LXAppearance-obconf to configure the look and feel.
  * link:https://bitbucket.org/fabriceT/obsession[obsession] for easy logout/shutdown/restart/etc tasks.
  * network-manager to manage network connections.
  * link:http://freedesktop.org/wiki/Software/LightDM/[LightDM] login manager with a simple greeter.
  * Themed Plymouth boot screen.
  * The Xfce terminal.
  * link:http://opensource.conformal.com/wiki/xombrero[Xombrero], a basic, webkit browser with vim keybinds.

=== Contact ===

#icebox on the link://freenode.net/[Freenode] IRC network
